## **Description**

This app provides quick news review. User can see all live top headlines. Clicking on headline he can read news detail, and with swipe to right he opens new screen. On that screen he can search for top articles on a topic he wants. List of articles will be displayed and also he can read more detail when article is selected.  

In order to have cleaner and more extensible code, architecture of this app is built in MVVM design pattern. All live data are supplied from internet, using REST API and connection with website: https://newsapi.org/  (Retrofit2 is used to consume REST API). For thumbnail images Glide library is used.

Guided by the principles of responsive UI design this app is customized for different screen sizes and resolutions.



## **Screenshots**

![screenshot1](https://gitlab.com/gogigox/newsoverview/-/blob/master/screenshots/Screenshot_20200913-100058.png)
<!--  -->
![screenshot2](https://gitlab.com/gogigox/newsoverview/-/blob/master/screenshots/Screenshot_20200913-100112.png)
<!--  -->
![screenshot3](https://gitlab.com/gogigox/newsoverview/-/blob/master/screenshots/Screenshot_20200913-100134.png)
<!--  -->
![screenshot4](https://gitlab.com/gogigox/newsoverview/-/blob/master/screenshots/Screenshot_20200913-100138.png)

package com.goran.newsoverview.adapters;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.goran.newsoverview.R;
import com.goran.newsoverview.model.Article;

import java.util.List;


public class ArticlesAdapter extends RecyclerView.Adapter<ArticlesAdapter.MyViewHolder> {


    private List<Article> results;

    public ArticlesAdapter.OnRVItemClick listenerArticlesList;

    public ArticlesAdapter.OnItemSwipe swipeListener;

    public interface OnItemSwipe {
        void onItemSwipe(MotionEvent event);
    }

    public interface OnRVItemClick {
        void onRVItemclick(Article article);
    }


    public ArticlesAdapter(List<Article> results, OnRVItemClick listenerArticlesList, OnItemSwipe swipeListener) {
        this.results = results;
        this.listenerArticlesList = listenerArticlesList;
        this.swipeListener = swipeListener;
    }


    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvTitle;
        private ImageView ivImage;
        View view;

        public MyViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            tvTitle = itemView.findViewById(R.id.tv_title);
            ivImage = itemView.findViewById(R.id.iv_image);

        }

        public void bind(final Article article, final ArticlesAdapter.OnRVItemClick listener,
                         final ArticlesAdapter.OnItemSwipe swipeListener) {

            try {
                if (article != null) {

                    tvTitle.setText(article.getTitle());

                    String imageUrl = article.getUrlToImage()
                            .replace("http://", "https://");

                    Glide.with(itemView)
                            .load(imageUrl)
                            .into(ivImage);

                }

            } catch (Exception e) {
                e.printStackTrace();
            }

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onRVItemclick(article);
                }
            });

            view.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    swipeListener.onItemSwipe(motionEvent);
                    return false;
                }
            });

        }
    }

    @NonNull
    @Override
    public ArticlesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.articles_single_item, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ArticlesAdapter.MyViewHolder myViewHolder, int i) {

        myViewHolder.bind(results.get(i), listenerArticlesList, swipeListener);
    }

    @Override
    public int getItemCount() {
        return results.size();
    }


    public void setResults(List<Article> results) {
        this.results = results;
        notifyDataSetChanged();
    }


}

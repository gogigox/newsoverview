package com.goran.newsoverview.api;

import com.goran.newsoverview.model.News;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface ArticlesService {

    @GET("everything")
    Call<News> getArticles(
            @Query("q") String keyword,
            @Query("apiKey") String apiKey

    );
}

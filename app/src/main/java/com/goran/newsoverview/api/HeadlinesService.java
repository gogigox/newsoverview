package com.goran.newsoverview.api;

import com.goran.newsoverview.model.News;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;


public interface HeadlinesService {

    @GET("top-headlines")
    Call<News> getHeadlines(
            @Query("country") String country,
            @Query("apiKey") String apiKey

    );

}

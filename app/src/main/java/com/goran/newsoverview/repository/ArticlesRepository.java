package com.goran.newsoverview.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.goran.newsoverview.api.ArticlesService;
import com.goran.newsoverview.model.News;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;


public class ArticlesRepository {

    public static final String API_KEY = "171b4cf7d70a45359df9519c75fffc80";
    private static final String BASE_URL = "https://newsapi.org/v2/";


    private ArticlesService articlesService;
    private MutableLiveData<News> articlesLiveData;


    public ArticlesRepository() {
        articlesLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        articlesService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ArticlesService.class);
    }


    public void getArticles(String ketword) {
       articlesService.getArticles(ketword, API_KEY)
                .enqueue(new Callback<News>() {
                    @Override
                    public void onResponse(@NotNull Call<News> call, @NotNull Response<News> response) {
                        if (response.body() != null) {
                            articlesLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<News> call, Throwable t) {
                        articlesLiveData.postValue(null);
                    }
                });
    }

    public LiveData<News> getArticlesLiveData() {
        return articlesLiveData;
    }

}

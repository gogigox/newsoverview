package com.goran.newsoverview.repository;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.goran.newsoverview.Utils;
import com.goran.newsoverview.api.HeadlinesService;
import com.goran.newsoverview.model.News;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.converter.gson.GsonConverterFactory;


public class HeadlinesRepository {


    public static final String API_KEY = "171b4cf7d70a45359df9519c75fffc80";
    private static final String BASE_URL = "https://newsapi.org/v2/";


    private HeadlinesService headlinesService;
    private MutableLiveData<News> headlinesLiveData;

    public HeadlinesRepository() {
        headlinesLiveData = new MutableLiveData<>();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.level(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

       headlinesService = new retrofit2.Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(HeadlinesService.class);
    }

    public void getHeadlines() {
        String country = Utils.getCountry();
        headlinesService.getHeadlines(country, API_KEY)
                .enqueue(new Callback<News>() {
                    @Override
                    public void onResponse(@NotNull Call<News> call, @NotNull Response<News> response) {
                        if (response.body() != null) {
                            headlinesLiveData.postValue(response.body());
                        }
                    }

                    @Override
                    public void onFailure(@NotNull Call<News> call, Throwable t) {
                        headlinesLiveData.postValue(null);
                    }
                });
    }

    public LiveData<News> getHeadlinesLiveData() {
        return headlinesLiveData;
    }

}

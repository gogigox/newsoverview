package com.goran.newsoverview.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.goran.newsoverview.MainActivity;
import com.goran.newsoverview.R;
import com.goran.newsoverview.adapters.ArticlesAdapter;
import com.goran.newsoverview.model.Article;
import com.goran.newsoverview.model.News;
import com.goran.newsoverview.viewmodel.ArticlesViewModel;

import java.util.ArrayList;
import java.util.List;


public class ArticlesActivity extends AppCompatActivity implements ArticlesAdapter.OnRVItemClick, GestureDetector.OnGestureListener,
        ArticlesAdapter.OnItemSwipe {


    float x1, x2, y1, y2;
    private GestureDetector gestureDetector;
    private EditText etKeyword;
    private Button btnSearch;

    private ArticlesViewModel viewModel;
    private ArticlesAdapter adapter;
    private List<Article> mArticlesList = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles);

        etKeyword = findViewById(R.id.et_keyword);
        btnSearch = findViewById(R.id.btn_search);

        this.gestureDetector = new GestureDetector(ArticlesActivity.this, this);

        adapter = new ArticlesAdapter(mArticlesList, this, this);

        viewModel = new ViewModelProvider(this).get(ArticlesViewModel.class);
        viewModel.init();
        viewModel.getArticlesLiveData().observe(this, new Observer<News>() {
            @Override
            public void onChanged(News news) {
                if (news != null) {
                    adapter.setResults(news.getArticles());
                }
            }
        });

        RecyclerView recyclerView = findViewById(R.id.rv_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        recyclerView.setAdapter(adapter);

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performSearch();
            }
        });
    }


    public void performSearch() {

        String mKeyword = etKeyword.getText().toString();
        viewModel.getArticles(mKeyword);
    }


    @Override
    public boolean onDown(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public void onShowPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onSingleTapUp(MotionEvent motionEvent) {
        return false;
    }

    @Override
    public boolean onScroll(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onLongPress(MotionEvent motionEvent) {

    }

    @Override
    public boolean onFling(MotionEvent motionEvent, MotionEvent motionEvent1, float v, float v1) {
        return false;
    }

    @Override
    public void onItemSwipe(MotionEvent event) {

        gestureDetector.onTouchEvent(event);
        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                x1 = event.getX();
                y1 = event.getY();

            case MotionEvent.ACTION_UP:
                x2 = event.getX();
                y2 = event.getY();

        }

        float valueX = x2 - x1;
        int minDistance = 100;
        if (Math.abs(valueX) > minDistance) {
            if (x2 > x1) {
                Intent intent = new Intent(ArticlesActivity.this, MainActivity.class);
                startActivity(intent);

            }

        }

    }

    @Override
    public void onRVItemclick(Article article) {
        Intent intent = new Intent(ArticlesActivity.this, ArticlesDetailActivity.class);

        intent.putExtra("title", article.getTitle());
        intent.putExtra("img", article.getUrlToImage());
        intent.putExtra("description", article.getDescription());
        startActivity(intent);

    }
}
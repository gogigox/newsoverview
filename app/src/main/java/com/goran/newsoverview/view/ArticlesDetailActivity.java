package com.goran.newsoverview.view;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.goran.newsoverview.R;


public class ArticlesDetailActivity extends AppCompatActivity {

    private TextView tvTitle, tvDescription;
    private ImageView ivImage;
    private String mTitle, mDescription, mImage;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_articles_detail);

        tvTitle = findViewById(R.id.tv_title);
        tvDescription = findViewById(R.id.tv_description);
        ivImage = findViewById(R.id.iv_image);

        setUpViews();
    }


    public void setUpViews() {

        Intent intent = getIntent();
        mTitle = intent.getStringExtra("title");
        mDescription = intent.getStringExtra("description");
        mImage = intent.getStringExtra("img");

        tvTitle.setText(mTitle);
        tvDescription.setText(mDescription);

        String imageUrl = mImage
                .replace("http://", "https://");

        Glide.with(getBaseContext())
                .load(imageUrl)
                .into(ivImage);


    }
}
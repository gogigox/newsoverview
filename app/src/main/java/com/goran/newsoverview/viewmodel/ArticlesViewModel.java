package com.goran.newsoverview.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.goran.newsoverview.model.News;
import com.goran.newsoverview.repository.ArticlesRepository;


public class ArticlesViewModel extends AndroidViewModel {

    private ArticlesRepository articlesRepository;
    private LiveData<News> articlesLiveData;

    public ArticlesViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        articlesRepository = new ArticlesRepository();
        articlesLiveData = articlesRepository.getArticlesLiveData();
    }


    public void getArticles(String keyword) {
        articlesRepository.getArticles(keyword);
    }


    public LiveData<News> getArticlesLiveData() {
        return articlesLiveData;
    }


}


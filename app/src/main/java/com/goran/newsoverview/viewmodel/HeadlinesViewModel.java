package com.goran.newsoverview.viewmodel;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.goran.newsoverview.model.News;
import com.goran.newsoverview.repository.HeadlinesRepository;


public class HeadlinesViewModel extends AndroidViewModel {

    private HeadlinesRepository headlinesRepository;
    private LiveData<News> headlinesLiveData;

    public HeadlinesViewModel(@NonNull Application application) {
        super(application);
    }

    public void init() {
        headlinesRepository = new HeadlinesRepository();
        headlinesLiveData = headlinesRepository.getHeadlinesLiveData();
    }


    public void getHeadlines()  {
        headlinesRepository.getHeadlines();
    }


    public LiveData<News> getHeadlinesLiveData() {
        return headlinesLiveData;
    }


}
